# Gidayu

## Features

* [x] Render a DFA
* [x] Render an NFA
* [x] Render a DFA computation
* [x] Render an NFA computation
* [x] Render an NFA-union
* [x] Render an NFA-concat
* [x] Render and NFA-star
* [x] Render an NFA-to-REGEX transformation step-by-step
* [ ] Render a REGEX-to-NFA transformation step-by-step
* [ ] Render an NFA-to-DFA transformation step-by-step
* [ ] Render a PDA
* [ ] Render a PDA computation

## Install

1. Install [poetry](https://python-poetry.org/)
2. Run `poetry install`

## Run examples

```bash
$ poetry run ./gidayu viz examples/lec11-ex2.json
$ poetry run ./gidayu run examples/lec12-ex2.yaml a a
```
