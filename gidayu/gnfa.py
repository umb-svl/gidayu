from pathlib import Path
from karakuri import regular
from karakuri.regular import WrapState_Start, WrapState_Mid, WrapState_End
import json
import yaml
import sys
from .common import FSMKind
from .transform import generate_sid
from .statediagram import FSM, State, Transition


class NoAliasDumper(yaml.SafeDumper):
    def ignore_aliases(self, data):
        return True

class GNFABuilder:
    def __init__(self, gnfa, start_sid, end_sid):
        self.gnfa = gnfa
        self.start_sid = start_sid
        self.end_sid = end_sid

    def get_sid(self, st):
        if isinstance(st, WrapState_Start):
            return self.start_sid
        elif isinstance(st, WrapState_End):
            return self.end_sid
        return st.state.sid

    def make_state(self, st):
        if isinstance(st, WrapState_Start):
            return State(
                sid=self.start_sid,
                highlight=False,
                initial=True,
                final=False,
                label="Q_I",
                transitions=[],
            )
        if isinstance(st, WrapState_End):
            return State(
                sid=self.end_sid,
                highlight=False,
                initial=False,
                final=True,
                label="Q_F",
                transitions=[],
            )
        if isinstance(st, WrapState_Mid):
            return State(
                sid=st.state.sid,
                initial=False,
                final=False,
                highlight=False,
                label=st.state.label,
                transitions=[],
            )

    def can_reduce(self):
        return self.gnfa.can_reduce()

    def get_state(self, st):
        return self.fsm.states[self.get_sid(st)]

    def get_edge(self, edge):
        src, dst = edge
        return self.fsm.transitions[(self.get_state(src), self.get_state(dst))]

    def build(self):
        self.fsm = fsm = FSM()
        for st in self.gnfa.states:
            fsm.add_state(self.make_state(st))

        for ((src, dst), char) in self.gnfa.transitions.items():
            fsm.add_transition(
                src = self.get_state(src),
                dst = self.get_state(dst),
                tsx = Transition(
                    highlight = False,
                    # Copy the style from old edge if it exists
                    style = [],
                    # Copy the topath from old edge if it exists
                    topath = [],
                    # Use the characters in the new FSM
                    chars = [char.to_string(nil_str="\\epsilon")],
                )
            )
    def reduce(self):
        self.build()
        state = self.gnfa.next_node()
        before, after = self.gnfa.next_edges(state)
        # Highlight the next node
        self.get_state(state).highlight = True
        for edge in before:
            self.get_edge(edge).highlight = True
        yield self.fsm
        # Advance the GNFA
        self.gnfa = self.gnfa.step()
        # Create a new FSM
        self.build()
        for edge in after:
            self.get_edge(edge).highlight = True
        yield self.fsm

class GNFA:
    def setup(self, parser):
        parser.add_argument("filename")
        parser.add_argument("--input-format", choices=["json", "yaml"], default="yaml")
        parser.add_argument("--output-format", default="yaml", choices=["json", "yaml"])
        parser.add_argument("-o", "--output-file", default="{{stem}}-gnfa-{{step}}.yaml")

    def nfa_to_gnfa(self, fsm):
        # Wrap state
        g = regular.GNFA.from_nfa(fsm.automaton)
        final = len(g.mid_states) + 1
        st2idx = {
            g.start_state: 0,
            g.end_state: final,
        }
        states = dict()
        states["q0"] = {"label": "s", "initial": True}
        for idx, s in enumerate(g.mid_states, 1):
            states[f"q{idx}"] = {"label": s.state.label}
            st2idx[s] = idx
        states[f"q{final}"] = {"label": "a", "final": True}
        transitions = []
        for ((src, dst), tsx) in g.transitions.items():
            src = st2idx[src]
            dst = st2idx[dst]
            transitions.append(
                dict(
                    src=f"q{src}",
                    dst=f"q{dst}",
                    actions=[tsx.serialize()]
                )
            )
        result = {
            "type": "gnfa",
            "states": states,
            "transitions": transitions,
        }
        return result

    def step(self, fsm, state=None):
        g = fsm.automaton
        if not g.can_reduce():
            print("GNFA cannot be reduced any further.")
            sys.exit(1)
        if state is None:
            idx = None
        else:
            idx = g.mid_states.index(state)
        g = g.step(idx)
        states = dict()
        states[g.start_state.sid] = {"label": g.start_state.label, "initial": True}
        states[g.end_state.sid] = {"label": g.end_state.label, "final": True}
        for s in g.mid_states:
            states[s.sid] = {"label": s.label}
        transitions = []
        for ((src, dst), tsx) in g.transitions.items():
            transitions.append(
                dict(
                    src=src.sid,
                    dst=dst.sid,
                    actions=[tsx.serialize()],
                )
            )
        return dict(type="gnfa", states=states, transitions=transitions)

    def run(self, args):
        fsm = FSM()
        with open(args.filename) as fp:
            fsm.load(fp)
        result = None
        if fsm.fsm_kind == FSMKind.NFA:
            # Wrap state
            result = self.nfa_to_gnfa(fsm)
        elif fsm.fsm_kind == FSMKind.GNFA:
            # step
            result = self.step(fsm)
        print(yaml.dump(result, Dumper=NoAliasDumper))
        # path = Path(args.filename)
        # base_dir = path.parent
        # stem = path.stem
        # for idx, fsm in enumerate(self.do_run(fsm.nfa, args)):
        #     output_file = base_dir / (generate_str(args.output_file, dict(stem=stem, step=idx)))
        #     fp_out = open(output_file, "w")
        #     data = fsm.build_dict()
        #     try:
        #         if args.output_format == "json":
        #             json.dump(data, fp_out)
        #         else:
        #             yaml.dump(data, fp_out)
        #     finally:
        #         if args.output_file is not None:
        #             fp_out.close()
