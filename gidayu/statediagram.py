import yaml

from karakuri import regular, contextfree
from pathlib import Path
from typing import Callable, Optional, Any, Iterable, Tuple, List
from graphviz import *
from operator import *
from dataclasses import dataclass, field, asdict
from enum import Enum
import os

from .common import (
    generate_pdf, generate_str, change_ext, OutputTarget, FSMKind
)
from .styler import GraphStyler, NodeStyler, EdgeStyler, Style, Modifiers


class RankDirection(Enum):
    TOP_TO_BOTTOM = 0
    BOTTOM_TO_TOP = 1
    LEFT_TO_RIGHT = 2
    RIGHT_TO_LEFT = 3
    @classmethod
    def from_string(cls, data):
        if data == 'LR':
            return cls.LEFT_TO_RIGHT
        elif data == 'RL':
            return cls.RIGHT_TO_LEFT
        elif data == 'TB':
            return cls.TOP_TO_BOTTOM
        elif data == 'BT':
            return cls.BOTTOM_TO_TOP
        else:
            raise ValueError(f"Unknown: {repr(data)}")
    def to_string(self):
        if self == self.TOP_TO_BOTTOM:
            return 'TB'
        elif self == self.BOTTOM_TO_TOP:
            return 'BT'
        elif self == self.LEFT_TO_RIGHT:
            return 'LR'
        elif self == self.RIGHT_TO_LEFT:
            return 'RL'

class Action:
    pass

def lt_opt_str(str1, str2):
    if str2 is None:
        return False
    if str1 is None:
        return True
    return str1 < str2


def build_action(fsm_kind:FSMKind) -> Callable[[Any],Action]:
    if fsm_kind == FSMKind.DFA or fsm_kind == FSMKind.NFA:
        return FAAction.load
    elif fsm_kind == FSMKind.PDA:
        return PDAAction.load
    elif fsm_kind == FSMKind.GNFA:
        return GNFAAction.load
    raise ValueError(f"Unexpected FSMKind: {fsm_kind}")

@dataclass(eq=True, frozen=True)
class FAAction(Action):
    read_char: Optional[str]
    kind:FSMKind = FSMKind.NFA

    @classmethod
    def load(cls, d):
        return cls(
            read_char = d,
        )

    def __lt__(self, other):
        return lt_opt_str(self.read_har, other.char)


@dataclass(eq=True, frozen=True)
class GNFAAction(Action):
    re: regular.Regex
    kind:FSMKind = FSMKind.GNFA

    @classmethod
    def load(cls, d):
        return cls(
            re = regular.Regex.from_dict(d),
        )

    def __lt__(self, other):
        return lt_opt_str(self.read_har, other.char)

@dataclass(eq=True, frozen=True)
class PDAAction(Action):
    read_char: Optional[str]
    push_char: Optional[str]
    pop_char: Optional[str]
    kind:FSMKind = FSMKind.PDA

    @classmethod
    def load(cls, d):
        return cls(
            push_char = d.get("push", None),
            read_char = d.get("read", None),
            pop_char = d.get("pop", None)
        )

    def __lt__(self, other):
        if self == other:
            return False
        for (e1, e2) in zip(self.nodes(), other.nodes):
            if e1 == e2:
                continue
            return lt_opt_str(e1, e2)

    def nodes(self):
        return (self.read_char, self.push_char, self.pop_char)

class StateModifier(Enum):
    INITIAL = 0
    FINAL = 1
    HIGHLIGHT = 2
    HIDE = 3

    def __str__(self):
        return self.name.lower()

@dataclass
class State:
    sid: str
    label: str = None
    initial: bool = False
    highlight: bool = False
    hide: bool = False
    final: bool = False
    style: list = field(default_factory=list)

    @classmethod
    def load(cls, sid, data):
        return cls(
            initial=data.get("initial", False),
            highlight=data.get("highlight", False),
            hide=data.get("hide", False),
            final=data.get("final", False),
            label=data.get("label", None),
            style=data.get("style", []),
            sid=sid,
        )
    def __eq__(self, other):
        if isinstance(other, State):
            return self.sid == other.sid
        return False

    def __hash__(self):
        return hash(self.sid)

    def __lt__(self, other):
        return self.sid < other.sid

    def get_modifiers(self):
        modifiers = []
        if self.initial:
            modifiers.append(StateModifier.INITIAL)
        if self.final:
            modifiers.append(StateModifier.FINAL)
        if self.highlight:
            modifiers.append(StateModifier.HIGHLIGHT)
        if self.hide:
            modifiers.append(StateModifier.HIDE)
        return list(map(str, modifiers))

    def get_context(self):
        return asdict(self)


class TransitionModifier(Enum):
    HIGHLIGHT = 1
    HIDE = 2

    def __str__(self):
        return self.name.lower()

@dataclass
class Transition:
    actions: list[Action]
    topath: list = field(default_factory=list)
    label_style: list = field(default_factory=list)
    style: list = field(default_factory=list)
    highlight: bool = False
    hide: bool = False

    @classmethod
    def load(cls, d, fsm_kind):
        action_factory = build_action(fsm_kind)
        actions = d.get("actions", None)
        if actions is None:
            actions = [action_factory(d)]
        else:
            actions = list(map(action_factory, actions))
        return cls(
            topath = d.get("topath", []),
            style = d.get("style", []),
            label_style = d.get("label_style", []),
            highlight = d.get("highlight", False),
            hide = d.get("hide", False),
            actions = actions,
        )

    def get_edge_modifiers(self):
        mods = []
        if self.highlight:
            mods.append(TransitionModifier.HIGHLIGHT)
        if self.hide:
            mods.append(TransitionModifier.HIDE)

        return list(map(str, mods))

    def get_context(self, src, dst):
        return dict(
            actions=self.actions,
            GNFA = FSMKind.GNFA,
            PDA = FSMKind.PDA,
            NFA = FSMKind.NFA,
            DFA = FSMKind.DFA,
            src = src.get_context(),
            dst = dst.get_context(),
            hide = self.hide,
            highlight = self.highlight
        )

class FSMBuilder:
    def add_state(self, st:State) -> None:
        raise NotImplementedError()
    def add_transition(self, src:State, dst:State, tsx:Transition) -> None:
        raise NotImplementedError()
    def build(self) -> Any:
        raise NotImplementedError()

class NFABuilder(FSMBuilder):
    def __init__(self):
        self.next_tsx = dict()
        self.alphabet = set()
        self.initial_state = None

    def add_state(self, st):
        if st.initial:
            if self.initial_state is not None:
                raise ValueError("Only one initial state")
            self.initial_state = st

    def add_transition(self, src, dst, tsx):
        for l in tsx.actions:
            char = l.read_char
            if char is not None:
               self.alphabet.add(char)
            out = self.next_tsx.get((src, char), None)
            if out is None:
                self.next_tsx[(src, char)] = out = []
            out.append(dst)

    def transition_func(self, st, char):
        return self.next_tsx.get((st, char), ())

    def build(self):
        if self.initial_state is None:
            raise ValueError("Error loading FSM: initial state missing.")
        return regular.NFA(
            alphabet = self.alphabet,
            start_state = self.initial_state,
            transition_func = self.transition_func,
            accepted_states=lambda x: x.final
        )


class GNFABuilder(FSMBuilder):
    def __init__(self):
        self.transitions = dict()
        self.start_state = None
        self.mid_states = []
        self.end_state = None
        self.alphabet = set()

    def add_state(self, st):
        if st.initial:
            if self.start_state is not None:
                raise ValueError("Only one initial state supported", st)
            self.start_state = st
        if st.final:
            if self.end_state is not None:
                raise ValueError("Only one final state supported", st)
            self.end_state = st
        if not st.final and not st.initial:
            self.mid_states.append(st)

    def add_transition(self, src, dst, tsx):
        if len(tsx.actions) != 1:
            raise ValueError("Exactly one action expected", tsx.actions)
        act, = tsx.actions
        self.alphabet.update(regular.get_alphabet(act.re))
        if (src, dst) in self.transitions:
            raise ValueError("No parallel transitions allowed", src, dst, tsx)
        self.transitions[(src, dst)] = act.re

    def build(self):
        if self.start_state is None:
            raise ValueError("Error loading FSM: initial state missing.")
        if self.end_state is None:
            raise ValueError("Error loading FSM: final state missing.")
        self.mid_states.sort(key=lambda x: x.sid)
        return regular.GNFA(
            alphabet = self.alphabet,
            mid_states = self.mid_states,
            start_state = self.start_state,
            end_state = self.end_state,
            transitions = self.transitions,
        )

class PDABuilder(FSMBuilder):
    def __init__(self):
        self.transitions = []
        self.initial_state = None

    def add_state(self, st):
        if st.initial:
            if self.initial_state is not None:
                raise ValueError("Only one initial state")
            self.initial_state = st

    def add_transition(self, src, dst, tsx):
        for lbl in tsx.actions:
            self.transitions.append(((src, lbl.read_char, lbl.pop_char), contextfree.Transition(lbl.push_char, dst)))

    def build(self):
        if self.initial_state is None:
            raise ValueError("Error loading FSM: initial state missing.")
        tsx = dict()
        for (k, t) in self.transitions:
            ts = tsx.get(k, None)
            if ts is None:
                tsx[k] = ts = []
            ts.append(t)
        return contextfree.PDA(
            start_state = self.initial_state,
            transitions = tsx,
            accepted_states=lambda x: x.final
        )

@staticmethod
def from_kind(fsm_kind):
    if fsm_kind == FSMKind.NFA or fsm_kind == FSMKind.DFA:
        return NFABuilder()
    elif fsm_kind == FSMKind.PDA:
        return PDABuilder()
    elif fsm_kind == FSMKind.GNFA:
        return GNFABuilder()
    raise ValueError(f"Unknown FSMKind: {fsm_kind}")

FSMBuilder.from_kind = from_kind
del from_kind

class FSM:
    def __init__(self):
        self.states = dict()
        self.transitions = dict()
        self.fsm_kind = None

    def add_state(self, st):
        self.states[st.sid] = st
        self.builder.add_state(st)

    def filter_states(self, pred):
        return [
            st
            for st in self.states.values()
            if pred(st)
        ]

    def get_highlighted_states(self):
        return self.filter_states(lambda x: x.highlight)

    def get_hidden_states(self):
        return self.filter_states(lambda x: x.hide)

    def filter_edges(self, pred):
        return [
            e
            for e in self.transitions.items()
            if pred(e)
        ]

    def get_highlighted_edges(self):
        self.filter_edges(lambda x: x[1].highlight)

    def get_hidden_edges(self):
        self.filter_edges(lambda x: x[1].hide)

    def get_nodes(self):
        return sorted(self.states.values())

    def get_edges(self):
            return sorted(self.transitions.items(), key=lambda x:x[0])

    def clear_highlights(self):
        for s in self.states.values():
            s.highlight = False
        for e in self.transitions.values():
            e.highlight = False

    def clear_hidden(self):
        for s in self.states.values():
            s.hide = False
        for e in self.transitions.values():
            e.hide = False

    def add_highlights(self, states:Iterable[str]=(), transitions:Iterable[Tuple[str, str]]=()):
        for s in states:
            self.states[s].highlight = True
        for (e1, e2) in transitions:
            edge = (self.states[e1], self.states[e2])
            self.transitions[edge].highlight = True

    def add_hidden(self, states:Iterable[str]=(), transitions:Iterable[Tuple[str, str]]=()):
        for s in states:
            self.states[s].hide = True
        for (e1, e2) in transitions:
            edge = (self.states[e1], self.states[e2])
            self.transitions[edge].hide = True

    def add_transition(self, src, dst, tsx):
        self.builder.add_transition(src, dst, tsx)
        self.transitions[(src, dst)] = tsx

    def build_automaton(self):
        self.automaton = self.builder.build()
        del self.builder

    def load(self, fp):
        data = yaml.safe_load(fp)
        self.fsm_kind = FSMKind.from_string(data.get("type", "nfa"))
        self.builder = FSMBuilder.from_kind(self.fsm_kind)
        for st, d in data.get("states", dict()).items():
            self.add_state(State.load(sid=st, data=d))
        for d in data.get("transitions", []):
            self.add_transition(
                src = self.states.get(d["src"], None),
                dst = self.states.get(d["dst"], None),
                tsx = Transition.load(d, self.fsm_kind),
            )
        self.build_automaton()


################


class StyleLoader:
    """
    Maps a configuration file into a GrahStyler
    """
    def get_style(self, *args):
        d = self.style
        for a in args:
            result = d.get(a, None)
            if result is None:
                return ()
            d = result
        return d

    def create_transition(self):
        tsx = self.style["state_diagram"]["transition"]
        tsx_label = tsx.get("label", dict())
        return EdgeStyler(
            format_label = tsx_label["format"],
            edge_modifiers = Modifiers.load(
                tsx,
                "highlight",
                "hide",
            ),
            label_modifiers = Modifiers.load(
                tsx_label,
            )
        )

    def create_state(self):
        state = self.style["state_diagram"].get("state", dict())
        state_label = state.get("label", dict())
        return NodeStyler(
            format_label=state_label["format"],
            modifiers=Modifiers.load(
                state,
                "highlight",
                "hide",
                "final",
                "initial",
            ),
        )

    def load(self, fp):
        self.style = yaml.safe_load(fp)
        return GraphStyler(
            d2t_options = self.style.get("options", None),
            d2t_graph_style = self.style.get("graph_style", None),
            document_preamble = self.style.get("document_preamble", None),
            figure_preamble = self.style.get("figure_preamble", None),
            direction = self.style.get("direction", "LR"),
            nodes = self.create_state(),
            edges = self.create_transition(),
        )


def build_dot(styler:GraphStyler, fsm):
    dot = Digraph()
    styler.setup(dot, fsm_kind=fsm.fsm_kind)
    for state in fsm.get_nodes():
        styler.nodes.add(
            dot=dot,
            nid=state.sid,
            style=Style(
                style=state.style,
                modifiers=state.get_modifiers(),
            ),
            context=state.get_context(),
        )
    for ((src, dst), e) in fsm.get_edges():
        styler.edges.add(
            dot=dot,
            src = src.sid,
            dst = dst.sid,
            context = e.get_context(src, dst),
            edge_style = Style(
                style=e.style,
                modifiers=e.get_edge_modifiers(),
            ),
            topath_style = Style(style=e.topath),
            label_style = Style(style=e.label_style)
        )
    return dot

def load_styler(fp):
    loader = StyleLoader()
    return loader.load(fp)


class StateDiagram:
    def setup(self, parser):
        parser.add_argument("filename")
        parser.add_argument("--disable-highlights",
            action="store_true",
            help="Disables hihglihgts in file."
        )
        parser.add_argument("--highlight-state", "-s",
            nargs="*",
            default=(),
            help=("State identifiers that need to highlight.")
        )
        parser.add_argument("--highlight-tsx", "-t",
            default=(),
            nargs="*",
            help="An edge is a pair of comma-separated state identifiers."
        )
        parser.add_argument("--disable-hidden",
            action="store_true",
            help="Disables hihglihgts in file."
        )
        parser.add_argument("--hide-state",
            nargs="*",
            default=(),
            help=("State identifiers that need to hide.")
        )
        parser.add_argument("--hide-tsx",
            default=(),
            nargs="*",
            help="An edge is a pair of comma-separated state identifiers."
        )
        OutputTarget.add_argparse(parser)

    def run(self, args):
        with open(args.filename) as fp:
            fsm = FSM()
            fsm.load(fp)

        style_path = os.path.join(os.path.dirname(__file__), "style.yaml")
        with open(style_path) as fp:
            styler = load_styler(fp)
        ######### Update hihglihgts ##########
        if args.disable_highlights:
            fsm.clear_highlights()
        edges = []
        for e in args.highlight_tsx:
            edge = e.split(",", 1)
            edges.append(edge)
        fsm.add_highlights(states=args.highlight_state, transitions=edges)
        ######### Update hidden ##########
        if args.disable_hidden:
            fsm.clear_hidden()
        edges = []
        for e in args.hide_tsx:
            edge = e.split(",", 1)
            edges.append(edge)
        fsm.add_hidden(states=args.hide_state, transitions=edges)
        #####################################
        dot = build_dot(styler, fsm)
        filename = change_ext(Path(args.filename), ".dot")
        dot.save(filename.name, directory=str(filename.parent))
        generate_pdf(filename, target=args.target)
