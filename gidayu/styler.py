import yaml

from dataclasses import dataclass, field
from typing import Optional, Dict, Collection, List, Tuple, Any

from graphviz import Digraph
from .common import FSMKind, generate_str


@dataclass(frozen=True)
class Style:
    modifiers:Collection[str]=()
    style:List[str]=field(default_factory=list)

@dataclass
class Modifiers:
    default:list[str] = field(default_factory=list)
    modifiers: List[Tuple[str, List[str]]] = field(default_factory=list)

    def build(self, style):
        curr_style = list(self.default)
        curr_style.extend(style.style)
        for modifier in style.modifiers:
            curr_style.extend(self.modifiers[modifier])
        if len(curr_style) > 0:
            return ",".join(curr_style)
        return None

    def add(self, data, key, style):
        curr_style = self.build(style)
        if curr_style is not None:
            data[key] = curr_style

    @classmethod
    def load(cls, data, *modifiers, **kwargs):
        """
        >>> d = dict(
        ... foo=["d"],
        ... hide=["h"],
        ... final=["f"],
        )
        >>> assert Modifers.load(d, "hide", "final") == Modifiers(
        ...     default=d.get("foo", []),
        ...     modifiers=dict(
        ...         hide=d.get("hide", []),
        ...         final=d.get("final", []),
        ...     ),
        ... ),
        True
        """
        if "default" in kwargs:
            default = kwargs["default"]
        else:
            default = data.get("default", [])
        # Nothing to load
        if default in modifiers:
            raise ValueError(f"Default {repr(default)} in modifiers={repr(modifiers)}")
        if data is None:
            return cls()
        # Something to load
        template = dict()
        for key in modifiers:
            template[key] = data.get(key, [])
        return cls(
                default=default,
                modifiers=template,
            )

@dataclass
class NodeStyler:
    format_label:str
    modifiers: Modifiers = field(default_factory=Modifiers)

    def add(self,
        dot:Digraph,
        nid:Any,
        context:Dict[str,Any]=dict(),
        style:Style=Style(),
    ):
        kwargs = dict()
        self.modifiers.add(kwargs, "style", style)
        dot.node(
            str(nid),
            label=generate_str(self.format_label, context),
            **kwargs
        )

@dataclass
class EdgeStyler:
    """A dot Edge"""
    format_label:str
    edge_modifiers: Modifiers = field(default_factory=Modifiers)
    label_modifiers: Modifiers = field(default_factory=Modifiers)
    topath_modifiers: Modifiers = field(default_factory=Modifiers)

    def add(self,
        dot:Digraph,
        src:Any,
        dst:Any,
        context:Dict[str,Any]=dict(),
        topath_style:Style=Style(),
        edge_style:Style=Style(),
        label_style:Style=Style(),
    ):
        kwargs = dict()
        self.edge_modifiers.add(kwargs, "style", edge_style)
        self.label_modifiers.add(kwargs, "lblstyle", label_style)
        self.topath_modifiers.add(kwargs, "topath", topath_style)
        dot.edge(
            str(src),
            str(dst),
            label=generate_str(self.format_label, context),
            **kwargs
        )

@dataclass
class GraphStyler:
    nodes:NodeStyler
    edges:EdgeStyler
    d2t_options:Optional[str] = None
    d2t_graph_style:Optional[str] = None
    figure_preamble:Optional[str] = None
    document_preamble:Optional[str] = None
    direction:Optional[str]=None

    def setup(self, dot, fsm_kind:FSMKind):
        if self.d2t_options is not None:
            dot.attr(d2toptions=self.d2t_options)
        if self.d2t_graph_style is not None:
            dot.attr(
                d2tgraphstyle=generate_str(
                    self.d2t_graph_style,
                    dict(fsm_kind=fsm_kind)
                )
            )
        if self.figure_preamble is not None:
            dot.attr(d2tfigpreamble=self.figure_preamble)
        if self.document_preamble is not None:
            dot.attr(d2tdocpreamble=self.document_preamble)
        if self.direction is not None:
            dot.attr(rankdir=self.direction)



    @classmethod
    def make(self, style, nodes, edges):
        return GraphStyler(
            d2t_options = style.get("options", None),
            d2t_graph_style = style.get("graph_style", None),
            document_preamble = style.get("document_preamble", None),
            figure_preamble = style.get("figure_preamble", None),
            direction = style.get("direction", "LR"),
            nodes = nodes,
            edges = edges,
        )

