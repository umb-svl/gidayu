from dataclasses import dataclass, asdict
from typing import List, Optional, Tuple
import yaml
from graphviz import Digraph
from pathlib import Path
import os

from .common import FSMKind, OutputTarget, generate_pdf, change_ext
from .styler import GraphStyler, NodeStyler, EdgeStyler, Modifiers, Style


@dataclass(frozen=True, eq=True)
class State:
    label: str
    sid: str
    final: bool = False
    initial: bool = False

@dataclass(eq=True)
class Configuration:
    cid: int
    state: State
    input: Tuple[str, ...]
    visible: bool = True
    initial: bool = False
    highlight: bool = False
    stack: Optional[Tuple[str, ...]] = None
    last: bool = False
    reaches_last: bool = False
    reaches_final: bool = False

    @property
    def final(self):
        return self.state.final and self.last 

    def get_modifiers(self):
        modifiers = []
        if self.initial:
            modifiers.append("initial")
        if self.last:
            modifiers.append("last")
        if self.final:
            modifiers.append("final")
        if not self.visible:
            modifiers.append("hide")
        if self.highlight:
            modifiers.append("highlight")
        if self.stack is not None:
            modifiers.append("has_stack")
        if self.reaches_last:
            modifiers.append("reaches_last")
        if self.reaches_final:
            modifiers.append("reaches_final")
        return modifiers

    def get_context(self):
        return asdict(self)

@dataclass
class Step:
    src: Configuration
    dst: Configuration
    read_char: Optional[str]
    pop_char: Optional[str]
    push_char: Optional[str]
    visible: bool = True
    highlight: bool = False

    @property
    def last(self):
        return self.dst.last
    @property
    def final(self):
        return self.dst.final

    @property
    def reaches_last(self):
        return self.dst.reaches_last

    @property
    def reaches_final(self):
        return self.dst.reaches_final

    def get_modifiers(self):
        modifiers = []
        if self.highlight:
            modifiers.append("highlight")
        if not self.visible:
            modifiers.append("hide")
        if self.reaches_final:
            modifiers.append("reaches_final")
        if self.reaches_last:
            modifiers.append("reaches_last")
        return modifiers

    def get_context(self):
        return asdict(self)

def get_forward_links(steps):
    links = dict()
    for s in steps:
        links_to = links.get(s.src.cid)
        if links_to is None:
            links[s.src.cid] = links_to = []
        links_to.append(s.dst)
    return links

def get_reverse_links(steps):
    links = dict()
    for s in steps:
        links_to = links.get(s.dst.cid)
        if links_to is None:
            links[s.dst.cid] = links_to = []
        links_to.append(s.src)
    return links

def reaches(steps, elements, links=None):
    to_visit = list(elements)
    if links is None:
        links = get_reverse_links(steps)
    visited = set(x.cid for x in to_visit)
    while len(to_visit) > 0:
        curr = to_visit.pop(0)
        yield curr
        for x in links.get(curr.cid, ()):
            if x.cid not in visited:
                to_visit.append(x)
                visited.add(x.cid)


@dataclass
class Computation:
    start: Configuration
    steps: List[Step]
    configurations: List[Step]
    input: List[str]

    def __post_init__(self):
        for c in reaches(self.steps, self.get_last_configurations()):
            c.reaches_last = True
        for c in reaches(self.steps, self.get_final_configurations()):
            c.reaches_final = True

    def get_last_configurations(self):
        for x in self.configurations:
            if x.last:
                yield x

    def get_final_configurations(self):
        for x in self.configurations:
            if x.final:
                yield x

    @classmethod
    def load(cls, data, dag=True):
        global_input = tuple(data["input"])
        states = dict()
        for (sid, s) in data["states"].items():
            states[sid] = State(
                sid=sid,
                label=s["label"],
                initial=s.get("initial", False),
                final=s.get("final", False),
            )
        visited = dict()
        configurations = []
        def mk_conf(n, initial=False): 
            state = states[n["sid"]]
            input = tuple(n["input"])
            stack = tuple(n["stack"]) if "stack" in n else None
            key = (state, input, stack)
            c = visited.get(key)
            if c is not None:
                return c
            else:
                visited[key] = c = Configuration(
                    cid = len(visited),
                    state = state,
                    input = input,
                    stack = stack,
                    initial = initial,
                    last = input == global_input,
                )
                configurations.append(c)
                return c
        start = mk_conf(data["start"], initial=True)
        steps = []
        for s in data["steps"]:
            src = mk_conf(s["src"])
            dst = mk_conf(s["dst"])
            if dag and src.cid > dst.cid:
                # Skip backward loops
                continue
            steps.append(Step(
                src = src,
                dst = dst,
                read_char = s.get("read"),
                pop_char = s.get("pop"),
                push_char = s.get("push"),
                visible = s.get("visible", True),
                highlight = s.get("highlight", False),
            ))
        return Computation(
            input = global_input,
            start = start,
            steps = steps,
            configurations = configurations,
        )


########################################################################
# Visualization



def make_graph_styler(fp):
    style = yaml.safe_load(fp)
    comp = style["computation_diagram"]
    conf = comp["configuration"]
    steps = comp["step"]
    return GraphStyler.make(
        style,
        nodes = NodeStyler(
            format_label=conf["label"]["format"],
            modifiers=Modifiers.load(
                conf,
                "initial",
                "final",
                "last",
                "has_stack",
                "reaches_last",
                "reaches_final",
                "highlight",
                "hide",
            ),
        ),
        edges = EdgeStyler(
            format_label = steps["label"]["format"],
            edge_modifiers = Modifiers.load(
                steps,
                "highlight",
                "reaches_last",
                "final",
                "last",
                "reaches_final",
                "hide",
            ),
            label_modifiers = Modifiers.load(steps["label"])
        ),
    )


def add_configuration(styler, dot,  configuration):
    styler.nodes.add(
        dot=dot,
        nid=configuration.cid,
        style=Style(
            modifiers=configuration.get_modifiers(),
        ),
        context=configuration.get_context(),
    )

def add_nodes(styler, dot, configurations, group_steps=True):
    if group_steps:
        confs = dict()
        for c in configurations:
            key = len(c.input)
            g = confs.get(key)
            if g is None:
                confs[key] = g = []
            g.append(c)

        for idx in sorted(confs):
            with dot.subgraph() as g:
                g.attr(rank="same")
                for conf in confs[idx]:
                    add_configuration(styler, g, conf)
    else:
        for c in configurations:
            add_configuration(styler, dot, c)


def add_edges(styler, dot, steps):
    for e in steps:
        styler.edges.add(
            dot=dot,
            src=e.src.cid,
            dst=e.dst.cid,
            context=e.get_context(),
            edge_style=Style(modifiers=e.get_modifiers()),
        )

def build_dot(styler:GraphStyler, computation, group_steps=True):
    dot = Digraph()
    if computation.start.stack:
        fsm_kind = FSMKind.NFA
    else:
        fsm_kind = FSMKind.PDA
    styler.setup(dot, fsm_kind=fsm_kind)
    add_nodes(styler, dot, computation.configurations, group_steps)
    add_edges(styler, dot, computation.steps)
    return dot



class ComputationViz:
    def setup(self, parser):
        parser.add_argument("filename")
        parser.add_argument("--ungroup-steps", dest="group_steps", action="store_false")
        parser.add_argument("--show-cycles", dest="dag", action="store_false") 
        OutputTarget.add_argparse(parser)

    def run(self, args):
        filename_in = Path(args.filename)
        with filename_in.open() as fp:
            data = yaml.safe_load(fp)
            computation = Computation.load(data, dag=args.dag)
        style_path = os.path.join(os.path.dirname(__file__), "style.yaml")
        with open(style_path) as fp:
            styler = make_graph_styler(fp)
        dot = build_dot(styler, computation, args.group_steps)
        filename_out = change_ext(filename_in, ".dot")
        dot.save(filename_out.name, directory=str(filename_in.parent))
        generate_pdf(filename_out, target=args.target)
