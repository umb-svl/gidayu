import subprocess
import jinja2
from pathlib import Path
import yaml
import sys
import enum

def change_ext(p, ext):
    return p.parent / (p.stem  + ext)

def load_tpl(tpl:str):
    env = jinja2.Environment(loader=jinja2.BaseLoader(), extensions=[])
    def flatten(iter):
        for elem in iter:
            if hasattr(elem, "__iter__"):
                yield from flatten(elem)
            else:
                yield elem
    def trim_par(x, empty=""):
        if isinstance(x, tuple) and len(x) == 1:
            return trim_par(x[0])
        if isinstance(x, tuple) and len(x) == 0:
            return empty
        x = str(x)
        if x.startswith("("):
            x = x[1:]
        if x.endswith(")"):
            x = x[:-1]
        return x
    def tt(x):
        if x is None:
            return None
        else:
            try:
                return "\\mathtt{" + x + "}"
            except TypeError as e:
                raise ValueError("Invalid argument", x) from e
    def char(x, epsilon="\epsilon"):
        if x is None:
            return epsilon
        else:
            return x
    def regex(x):
        return x.to_string(
            void_str="\\emptyset",
            nil_str="\\epsilon",
            union_str=lambda x, y: x + " \\cup " + y,
            star_str=lambda x: x + " ^\\star",
            char_str=lambda x: "{\\tt " + x + "}"
        )

    env.filters.update({
        'regex': regex,
        'trim_par': trim_par,
        'char': char,
        'tt': tt,
    })
    return env.from_string(tpl)

def generate_str(template, data):
    tpl = load_tpl(template)
    return tpl.render(**data)

class OutputTarget(enum.Enum):
    DOT = 0
    TEX = 1
    PDF = 2
    VIEW = 3

    def __str__(self):
        return self.name.lower()

    def __repr__(self):
        return str(self)

    @classmethod
    def get_names(cls):
        return list((k.lower() for k in cls.__members__.keys()))

    @classmethod
    def parse(cls, k):
        try:
            return cls.from_name(k.upper())
        except KeyError:
            return k

    @classmethod
    def from_name(cls, k):
        return cls.__members__[k]

    @classmethod
    def add_argparse(cls, parser, short_option=None, long_option="--target"):
        args = []
        if short_option is not None:
            args.append(short_option)
        if long_option is not None:
            args.append(long_option)
        parser.add_argument(*args,
            choices=list(cls),
            type=OutputTarget.parse,
            default=OutputTarget.PDF,
            help="Add output target. Default %(default)s"
        )

class Exec:
    def __init__(self, target):
        self.temporary_files = []
        self.target = target

    def run(self, cmd, expected, filename=None):
        if self.target.value < expected.value:
            return
        if self.target.value > expected.value and filename is not None:
            self.temporary_files.append(filename)
        try:
            subprocess.run(
                cmd,
                check=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL
            )
        except subprocess.CalledProcessError as e:
            subprocess.run(cmd)
            raise e

    def cleanup(self, missing_ok=True):
        for f in self.temporary_files:
            f.unlink(missing_ok=missing_ok)



def generate_pdf(filename, target):
    dot_file = Path(filename)
    tex_file = change_ext(dot_file, ".tex")
    pdf_file = change_ext(dot_file, ".pdf")
    # Generate TeX file:
    runner = Exec(target)
    try:
        # Generate dot file
        runner.run(
            ["dot2tex", dot_file, "-o", tex_file],
            expected=OutputTarget.DOT,
            filename=dot_file
        )
        # Generate Tex
        runner.run(
            ["latexmk",  tex_file.name, "-cd", "-pdf", "-output-directory=" + str(tex_file.parent)],
            expected=OutputTarget.TEX,
            filename=tex_file
        )
        # Generate PDF:
        runner.run(
            ["latexmk",  tex_file.name, "-cd", "-pdf", "-c", "-output-directory=" + str(tex_file.parent)],
            expected=OutputTarget.PDF,
            filename=tex_file
        )
        runner.run(
            ["latexmk",  tex_file.name, "-cd", "-pdf", "-output-directory=" + str(tex_file.parent)],
            expected=OutputTarget.PDF,
            filename=tex_file
        )
        # Show PDF
        runner.run(
            ["xdg-open", pdf_file],
            expected=OutputTarget.VIEW,
            filename=pdf_file
        )
    except subprocess.CalledProcessError as e:
        sys.exit(e.returncode)
    finally:
        # Clean temporary files:
        runner.cleanup()


def file_loader(filename, input_format):
    if filename == "-":
        fp = sys.stdin
    else:
        fp = open(filename)
    try:
        if input_format == "yaml":
            return yaml.safe_load(fp)
        else:
            return json.load(fp)
    finally:
        if filename != "-":
            fp.close()



class FSMKind(enum.Enum):
    NFA = 0
    DFA = 1
    GNFA = 2
    PDA = 3

    def __str__(self):
        if self == FSMKind.NFA:
            return "nfa"
        elif self == FSMKind.DFA:
            return "dfa"
        elif self == FSMKind.PDA:
            return "pda"
        elif self == FSMKind.GNFA:
            return "gnfa"
        else:
            raise ValueError(f"Unsupported FSMKind: {self}")

    @classmethod
    def from_string(cls, kind:str):
        kind = kind.lower().strip()
        if kind == "nfa":
            return cls.NFA
        elif kind == "dfa":
            return cls.DFA
        if kind == "gnfa":
            return cls.GNFA
        elif kind == "pda":
            return cls.PDA
        return ValueError(f"Unknown: {kind}")
