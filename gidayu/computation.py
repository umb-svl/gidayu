from typing import Any
from karakuri import regular, contextfree
from pathlib import Path
from graphviz import Digraph
from dataclasses import dataclass, field
import os

from .common import generate_pdf, change_ext, OutputTarget, FSMKind
from .statediagram import FSM, State, Transition, FAAction, load_styler


class Node:
    def __init__(self, nid, state, is_final):
        self.nid = nid
        self.state = state
        self.is_final = is_final
        self.connects_to = []
        self.closure = []
        self.char = None
        self.reaches = None
        self.is_active = None
        self.is_initial = False
        self.is_frontier = False

    def update_reaches(self):
        self.reaches = set()
        for x in self.connects_to:
            self.reaches.add(x)
        for x in self.closure:
            self.reaches.add(x)
        for x in list(self.reaches):
            x.update_reaches()
            self.reaches.update(x.reaches)
        for x in self.reaches:
            if x.is_final:
                self.is_active = True
                break
        else:
            # No break happened
            self.is_active = self.is_final

    def link(self, char, node):
        if char is None:
            self.closure.append(node)
        else:
            self.char = char
            self.connects_to.append(node)

    def update_words(self, word=None, step=None):
        if step is None:
            step = 0
        if word is None:
            word = ()
        self.word = word
        self.step = step
        step += 1
        for n in self.closure:
            n.update_words(word, step)
        word = word + (self.char,)
        step += 1
        for n in self.connects_to:
            n.update_words(word, step)

    def __lt__(self, other):
        return self.nid < other.nid

    def __repr__(self):
        return f"Node(state={self.state}, active={self.is_active})"

    def as_state(self):
        if isinstance(self.state, contextfree.StackState):
            label = self.state.state.label
            stack = self.state.stack
        else:
            label = self.state.label
            stack = None

        return State(
            sid=str(self.nid),
            label=label,
            initial=self.is_initial,
            final=self.is_final,
            active=self.is_active,
            highlight=self.is_frontier,
            stack=stack,
        )

@dataclass
class Edge:
    source: Node
    char: Any
    target: Node

@dataclass
class Step:
    char: Any
    edges: list


def pda_as_nfa(pda:contextfree.PDA):
    return regular.NFA(
        start_state = pda.start_stack_state,
        accepted_states = lambda x: pda.accepted_states(x.state),
        transition_func = pda.transition_func,
        alphabet = pda.alphabet,
    )

class Computation:
    def __init__(self, fsm_kind, final):
        self.fsm_kind = fsm_kind
        self.nodes = dict()
        self.nid = 0
        self.final = final
        self.root = None
        self.steps = []
        self.frontier = None

    def add_node(self, state):
        node = self.nodes.get(state, None)
        if node is None:
            self.nodes[state] = node = Node(
                nid=str(self.nid),
                state=state[0],
                is_final=state in self.final,
            )
            self.nid += 1
        return node

    def add_edge(self, edge):
        node1 = self.add_node(edge.source)
        node2 = self.add_node(edge.target)
        node1.link(edge.edge, node2)
        return (node1, node2)

    def add_step(self, edges):
        if self.root is None:
            self.root = self.add_node(edges[0].source)
            self.is_initial = True
        edges = [ self.add_edge(e) for e in edges ]
        # get the first edge; get the source node
        char = edges[0][0].char
        self.expect_epsilon = char is not None
        self.steps.append(
            Step(
                char=char,
                edges=edges,
            )
        )
    def _set_step_is_frontier(self, idx, v):
        """
        Updates the is_frontier flag of a given step.
        """
        for (_, dst) in self.steps[idx].edges:
            dst.is_frontier = v

    def _set_frontier(self, step):
        if self.frontier is not None:
            # Clear last frontier set
            self._set_step_is_frontier(self.frontier, False)
        self.frontier = step
        self._set_step_is_frontier(step, True)
        # When a step targets an epsilon, set the previous step as frontier
        if self.steps[step].char is None and step - 1 >= 0:
            self._set_step_is_frontier(step - 1, True)

    def get_string(self):
        return [s.char for s in self.steps if s.char is not None]

    def update_frontier(self):
        self._set_frontier(len(self.steps) - 1)

    def set_frontier(self, step):
        # Reset frontier
        self._set_frontier(step)

    def get_edges(self):
        for step in self.steps:
            yield from step.edges

    def get_frontier_states(self):
        return set((n.state
            for n in self.nodes.values()
            if n.is_frontier
        ))

    def get_nodes(self):
        return iter(self.nodes.values())

    def __len__(self):
        return len(self.nodes)

    def __repr__(self):
        return repr(list(self))

    @classmethod
    def make_from(cls, fsm_kind, start_state, steps, final):
        db = cls(fsm_kind, final)
        db.root = db.add_node((start_state, 0))
        db.root.is_initial = True
        for s in steps:
            if len(s) == 0:
                break
            db.add_step(s)
        if len(db.steps) > 0:
            db.update_frontier()
            db.root.update_reaches()
        return db

    def to_dot(self, styler):
        dot = Digraph()
        styler.setup(dot, self.fsm_kind)
        for node in self.get_nodes():
            styler.add_state(dot, node.as_state())
        for step in self.steps:
            for (src, dst) in step.edges:
                styler.add_transition(
                    dot,
                    src = src.as_state(),
                    dst = dst.as_state(),
                    tsx = Transition(
                        highlight=dst.is_frontier,
                        active=dst.is_active,
                        labels=[FALabel(char=step.char)],
                    )
                )
        return dot

    @classmethod
    def make(cls, fsm, word, step=None):
        if fsm.fsm_kind == FSMKind.GNFA:
            raise ValueError("GNFAs unsupported")
        if fsm.fsm_kind == FSMKind.PDA:
            fa = pda_as_nfa(fsm.automaton)
        else:
            fa = fsm.automaton
        edges, final = regular.nfa_derivation_graph(fa, word)
        if step is not None:
            edges = edges[0:step]
        return cls.make_from(fsm.fsm_kind, fa.start_state, edges, final)




class Run:
    def setup(self, parser):
        parser.add_argument("filename")
        parser.add_argument("--step", type=int, default=None)
        parser.add_argument("--list-steps", action="store_true", help="Print out the number of steps and returns.")
        parser.add_argument("--list-frontier", action="store_true", help="Print out the states in the frontier, one per line.")
        parser.add_argument("char", default=[], help="Renders the reduction of accepting/rejecting a word", nargs="*")
        OutputTarget.add_argparse(parser)

    def run(self, args):
        filename_in = Path(args.filename)
        with filename_in.open() as fp:
            fsm = FSM()
            fsm.load(fp)
        style_path = os.path.join(os.path.dirname(__file__), "style.yaml")
        with open(style_path) as fp:
            styler = load_styler(fp)
        computation = Computation.make(fsm, args.char, step=args.step)
        if args.list_steps:
            print(len(computation.steps))
            return
        if args.list_frontier:
            if args.step is not None and args.step == 0:
                print(fsm.initial_state.sid)
                return
            for st in sorted(computation.get_frontier_states(), key=lambda x:x.sid):
                print(st.sid)
            return
        dot = computation.to_dot(styler)
        filename_out = change_ext(filename_in, ".dot")
        dot.save(filename_out.name, directory=str(filename_in.parent))
        generate_pdf(filename_out, target=args.target)
