#!/usr/bin/env python3
import sys
import argparse
import yaml
import json
from karakuri.regsets import RegularSet
from .statediagram import StateDiagram
from .run import Run
from .gnfa import GNFA
from .transform import NFAConcat, NFAUnion, NFAStar
from .computationviz import ComputationViz
################################################################################

OPERATIONS = {
    "complement": RegularSet.complement,
    "minimize": RegularSet.minimize,
    "star": RegularSet.star,
    "remove_epsilon": RegularSet.remove_epsilon_transitions,
    "remove_sink": RegularSet.remove_sink_states,
}
OPERATION_KEYS = list(sorted(OPERATIONS.keys()))

BINOP = {
    "union": RegularSet.union,
    "intersect": RegularSet.union,
    "product": RegularSet.product,
    "subtract": RegularSet.subtract,
    "shuffle": RegularSet.shuffle,
    "concat": RegularSet.concat
}
BINOP_KEYS = list(sorted(BINOP.keys()))

class Binary:
    def setup(self, parser):
        parser.add_argument("op", choices=BINOP_KEYS)
        parser.add_argument("filename1")
        parser.add_argument("filename2")
        parser.add_argument("--input-format-1", choices=["json", "yaml"], default="yaml")
        parser.add_argument("--input-type-1", choices=["nfa", "dfa"], default="nfa")
        parser.add_argument("--input-format-2", choices=["json", "yaml"], default="yaml")
        parser.add_argument("--input-type-2", choices=["nfa", "dfa"], default="nfa")
        parser.add_argument("--output-type", default="nfa", choices=["nfa", "dfa"])
        parser.add_argument("--output-format", default="yaml", choices=["json", "yaml"])
        parser.add_argument("-o", "--output-file")

    def run(self, args):
        if args.filename1 == "-":
            fp = sys.stdin
        else:
            fp = open(args.filename1)
        try:
            if args.input_format_1 == "yaml":
                data1 = yaml.safe_load(fp)
            else:
                data1 = json.load(fp)
        finally:
            if args.filename1 != "-":
                fp.close()
        with open(args.filename2) as fp:
            if args.input_format_2 == "yaml":
                data2 = yaml.safe_load(fp)
            else:
                data2 = json.load(fp)
        if args.input_type_1 == "nfa":
            rset1 = RegularSet.make_nfa_from_dict(data1)
        else:
            rset1 = RegularSet.make_dfa_from_dict(data1)
        if args.input_type_2 == "nfa":
            rset2 = RegularSet.make_nfa_from_dict(data2)
        else:
            rset2 = RegularSet.make_dfa_from_dict(data2)
        rset = BINOP[args.op](rset1, rset2)
        if args.output_type == "nfa":
            data = rset.get_nfa().as_dict(flatten=False)
        else:
            data = rset.get_dfa().as_dict(flatten=False)
        if args.output_file is not None:
            fp_out = open(args.output_file, "w")
        else:
            fp_out = sys.stdout
        try:
            if args.output_format == "json":
                json.dump(data, fp_out)
            else:
                yaml.dump(data, fp_out)
        finally:
            if args.output_file is not None:
                fp_out.close()

class Unary:
    def setup(self, parser):
        parser.add_argument("filename")
        parser.add_argument("--input-format", choices=["json", "yaml"], default="yaml")
        parser.add_argument("--input-type", choices=["nfa", "dfa"], default="nfa")
        parser.add_argument("--apply", "-a", choices=OPERATION_KEYS, nargs="*", default=[])
        parser.add_argument("--output-type", default="nfa", choices=["nfa", "dfa"])
        parser.add_argument("--output-format", default="yaml", choices=["json", "yaml"])
        parser.add_argument("-o", "--output-file")
    def run(self, args):
        if args.filename == '-':
            fp = sys.stdin
        else:
            fp = open(args.filename)
        try:
            if args.input_format == "yaml":
                data = yaml.safe_load(fp)
            else:
                data = json.load(fp)
        finally:
            if args.filename == '-':
                fp.close()
        if args.input_type == "nfa":
            rset = RegularSet.make_nfa_from_dict(data)
        else:
            rset = RegularSet.make_dfa_from_dict(data)
        for p in args.apply:
            rset = OPERATIONS[p](rset)
        if args.output_type == "nfa":
            data = rset.get_nfa().as_dict(flatten=False)
        else:
            data = rset.get_dfa().as_dict(flatten=False)
        if args.output_file is not None:
            fp_out = open(args.output_file, "w")
        else:
            fp_out = sys.stdout
        try:
            if args.output_format == "json":
                json.dump(data, fp_out)
            else:
                yaml.dump(data, fp_out)
        finally:
            if args.output_file is not None:
                fp_out.close()


def parse_transition(data):
    l, r = data.split(",")
    return (int(l), int(r))

def main():
    parser = argparse.ArgumentParser()
    unary = Unary()
    binary = Binary()
    subparsers = {
        "viz": StateDiagram(),
        "run": Run(),
        "cat": NFAConcat(),
        "union": NFAUnion(),
        "star": NFAStar(),
        "gnfa": GNFA(),
        "cviz": ComputationViz(),
    }
    sub = parser.add_subparsers(dest="command", required=True)
    for (name, handler) in subparsers.items():
        handler.setup(sub.add_parser(name))
    args = parser.parse_args()
    subparsers[args.command].run(args)

if __name__ == '__main__':
    main()
