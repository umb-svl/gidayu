from dataclasses import dataclass, field
from typing import Tuple, List, Optional
from karakuri import regular, contextfree
from pathlib import Path
import yaml
from .statediagram import FSM, FSMKind


def consume_fuel(x):
    if x is None:
        return None
    return x - 1

@dataclass(frozen=True, eq=True, order=True)
class Configuration:
    index: int
    state: str
    fuel: Optional[int] = None
    def get_next(self, tsx, accepted, word):
        if self.fuel is not None and self.fuel <= 0:
            return
        fuel = consume_fuel(self.fuel)
        if self.index < len(word):
            for x in tsx(self.state, word[self.index]):
                yield Configuration(
                    index = self.index + 1,
                    state = x,
                    fuel = fuel
                )
        for x in tsx(self.state, None):
            yield Configuration(
                index = self.index,
                state = x,
                fuel = fuel,
            )

    def get_input(self, word):
        return word[:self.index]

    def get_key(self, word):
        return (word[:self.index], self.state.sid)


    def to_dict(self, fsm_kind, word):
        if fsm_kind == FSMKind.PDA:
            s = self.state.state
        else:
            s = self.state
        d = dict(
            input = self.get_input(word),
            sid = s.sid,
        )
        if fsm_kind == FSMKind.PDA:
            d["stack"] = list(self.state.stack)
        if s.final and len(word) == self.index:
            d["final"] = True
        return d

    @classmethod
    def make_start(cls, automaton, word):
        if isinstance(automaton, regular.NFA) or isinstance(automaton, regular.DFA):
            fuel = None
            start = automaton.start_state
        if isinstance(automaton, contextfree.PDA):
            states = len(automaton.states) + 1
            fuel = (len(word) + 1) * states * states
            start = automaton.start_stack_state
        return cls(state = start, index = 0, fuel = fuel)


@dataclass
class Step:
    src: Configuration
    dst: Configuration

    def get_read(self, word):
        if self.src.index != self.dst.index and self.src.index + 1 != self.dst.index:
            raise ValueError()
        if self.src.index == self.dst.index:
            return None
        return word[self.src.index]

    def __iter__(self):
        yield self.src
        yield self.dst


    def get_push(self):
        src = self.src.state.stack
        dst = self.dst.state.stack
        if len(src) < len(dst):
            return dst[-1]
        elif len(src) == len(dst) and len(src) > 0 and src[-1] != dst[-1]:
            return dst[-1]
        return None

    def get_pop(self):
        src = self.src.state.stack
        dst = self.dst.state.stack
        if len(src) > len(dst):
            return src[-1]
        elif len(src) > 0 and len(src) == len(dst) and src[-1] != dst[-1]:
            return dst[-1]
        return None

    def to_dict(self, fsm_kind, word):
        data = dict(
            src=self.src.to_dict(fsm_kind, word),
            dst=self.dst.to_dict(fsm_kind, word),
            read=self.get_read(word),
        )
        if fsm_kind == FSMKind.PDA:
            # Pop
            pop = self.get_pop()
            if pop is not None:
                data["pop"] = pop
            # Push
            push = self.get_push()
            if push is not None:
                data["push"] = push

        return data

def run_computation(start, automaton, word):
    word = tuple(word)
    nodes = [start]
    visited = set([start])
    while len(nodes) > 0:
        curr = nodes[0]
        del nodes[0]
        for x in curr.get_next(automaton.transition_func, automaton.accepted_states, word):
            yield Step(curr, x)
            if x not in visited:
                nodes.append(x)
                visited.add(x)

@dataclass
class Computation:
    fsm: FSM
    input: list[str]
    steps: List[Step] = field(init=False)
    start: Configuration = field(init=False)
    configurations: List[Configuration] = field(init=False)

    def __post_init__(self):
        self.start = Configuration.make_start(
            self.fsm.automaton,
            self.input
        )
        self.steps = list(run_computation(
            self.start,
            self.fsm.automaton,
            self.input,
        ))
        states = [self.start.state]
        visited = set([self.start.state])
        for s in self.steps:
            for n in s:
                if n.state not in visited:
                    visited.add(n.state)
                    states.append(n.state)
        self.configurations = list(states)

    def to_dict(self):
        def node_to_st(s):
            if self.fsm.fsm_kind == FSMKind.PDA:
                s = s.state
            d = dict(label=s.label)
            if s.final:
                d["final"] = True
            if s.initial:
                d["initial"] = True
            return (s.sid, d)
        states = dict(map(node_to_st, self.configurations))
        return dict(
            input = self.input,
            start = self.start.to_dict(self.fsm.fsm_kind, self.input),
            states = states,
            steps = [ s.to_dict(self.fsm.fsm_kind, self.input) for s in self.steps ],
        )

##########################################################################################
# CLI

# def do_stuff(filename, word):
#     fsm = FSM()
#     with open(filename) as fp:
#         fsm.load(fp)
#     c = Computation(fsm, word)
#     import yaml
#     print(yaml.dump(c.to_dict()))

# import sys
# do_stuff(sys.argv[1], sys.argv[2:])


class Run:
    def setup(self, parser):
        parser.add_argument("filein")
        parser.add_argument("fileout")
        parser.add_argument("--dag", action="store_true", help="Skip any step that introduces a cycle.")
        parser.add_argument("char", default=[], help="Renders the reduction of accepting/rejecting a word", nargs="*")

    def run(self, args):
        filename_in = Path(args.filein)
        with filename_in.open() as fp:
            fsm = FSM()
            fsm.load(fp)
        c = Computation(fsm, args.char)
        with open(args.fileout, "w") as fp:
            yaml.dump(c.to_dict(), fp)
