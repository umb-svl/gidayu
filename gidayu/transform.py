import sys
import yaml

from dataclasses import dataclass, field
from typing import Callable
from karakuri import regular
from karakuri.regular import StarState_Init, ConcatState_Left
from enum import Enum
import os
from .statediagram import FSM, State, Transition
from .common import generate_str


def generate_sid(names):
    """
    Generates a fresh state identifier from an existing FSM.
    """
    sid = 0
    while True:
        name = "q" + str(sid)
        if name not in names:
            return name
        sid += 1

@dataclass
class UnaryOpNFA:

    operation : Callable[[FSM], dict]
    def setup(self, parser):
        parser.add_argument("filename")
        parser.add_argument("--input-format", choices=["json", "yaml"], default="yaml")
        parser.add_argument("--output-format", default="yaml", choices=["json", "yaml"])
        parser.add_argument("-o", "--output-file")
        parser.add_argument("--star-state", "-s",
            default=r"q_\star",
            metavar="LABEL",
            help="Label of the state added by the star operation.\n Default: %(default)s")

    def run(self, args):
        with open(args.filename) as fp:
            fsm = FSM()
            fsm.load(fp)
        data = self.operation(fsm, args)
        if args.output_file is not None:
            fp_out = open(args.output_file, "w")
        else:
            fp_out = sys.stdout
        try:
            if args.output_format == "json":
                json.dump(data, fp_out)
            else:
                yaml.dump(data, fp_out)
        finally:
            if args.output_file is not None:
                fp_out.close()

@dataclass
class BinaryOpNFA:
    operation : Callable[[regular.NFA, regular.NFA], dict]
    def setup(self, parser):
        parser.add_argument("filename1")
        parser.add_argument("filename2")
        parser.add_argument("--output-format", default="yaml", choices=["json", "yaml"])
        parser.add_argument("-o", "--output-file")

    def run(self, args):
        with open(args.filename1) as fp1, open(args.filename2) as fp2:
            fsm1 = FSM()
            fsm1.load(fp1)
            fsm2 = FSM()
            fsm2.load(fp2)
        style_path = os.path.join(os.path.dirname(__file__), "style.yaml")
        with open(style_path) as fp:
            style = yaml.safe_load(fp)
        data = self.operation(style, fsm1, fsm2)
        if args.output_file is not None:
            fp_out = open(args.output_file, "w")
        else:
            fp_out = sys.stdout
        try:
            if args.output_format == "json":
                json.dump(data, fp_out)
            else:
                yaml.dump(data, fp_out)
        finally:
            if args.output_file is not None:
                fp_out.close()


class BinOpBuilder:
    def __init__(self, style, fsm1, fsm2):
        self.fsm1 = fsm1
        self.fsm2 = fsm2
        self.nfa3 = self.binop(fsm1.nfa, fsm2.nfa)
        self.new_fsm = FSM()
        self._post_init(style)

    def add_state(self, st):
        self.new_fsm.add_state(self.wrap_state(st).build())

    def get_state(self, st):
        return self.new_fsm.states[st.sid]

    def add_transition(self, src, dst, chars):
        src_dir = self.wrap_state(src)
        dst_dir = self.wrap_state(dst)
        old_tsx = self.get_old_tsx(src_dir, dst_dir)
        self.new_fsm.add_transition(
            src = self.get_state(src_dir),
            dst = self.get_state(dst_dir),
            tsx = Transition(
                # Highlight when we are connecting left with right
                highlight = self.highlight(src_dir, dst_dir),
                # Copy the style from old edge if it exists
                style = list(old_tsx.style) if old_tsx is not None else None,
                # Copy the topath from old edge if it exists
                topath = list(old_tsx.topath) if old_tsx is not None else None,
                # Use the characters in the new FSM
                chars = list(chars),
            )
        )

    def build(self):
        for st in self.nfa3.states:
            self.add_state(st)

        for ((src, dst), chars) in self.nfa3.as_graph()[1].items():
            self.add_transition(src, dst, chars)

        return self.new_fsm

    @classmethod
    def run(cls, style, fsm1, fsm2):
        # Create this builder
        builder = cls(style, fsm1, fsm2)
        # Creates the FSM
        new_fsm = builder.build()
        # Convert the FSM into a dictionary
        return new_fsm.build_dict()
    # Extension points:

    def _post_init(self, style):
        pass

    def binop(self, nfa1, nfa2):
        raise NotImplementedError()

    def wrap_state(self, st):
        raise NotImplementedError()

    def get_old_tsx(self, src, dst):
        raise NotImplementedError()

    def highlight(self, src, dst):
        raise NotImplementedError

class ConcatBuilder(BinOpBuilder):
    def _post_init(self, style):
        self.left = style.get("concatenate", dict()).get("left_state", "{{ _ }}")
        self.right = style.get("concatenate", dict()).get("right_state", "{{ _ }}")

    def binop(self, nfa1, nfa2):
        return nfa1.concat(nfa2)

    def wrap_state(self, st):
        return ConcatState.make(self, st)

    def get_old_tsx(self, src_dir, dst_dir):
        edge = (src_dir.state, dst_dir.state)
        if src_dir.is_left and dst_dir.is_left:
            old_tsx = self.fsm1.transitions.get(edge)
        elif src_dir.is_right and dst_dir.is_right:
            old_tsx = self.fsm2.transitions.get(edge)
        else:
            old_tsx = None
        return old_tsx

    def highlight(self, src_dir, dst_dir):
        # Highlight when we are connecting left with right
        return src_dir.is_left != dst_dir.is_left

@dataclass
class ConcatState:
    builder: ConcatBuilder
    state: State
    is_left: bool
    is_right: bool
    sid:str = field(init=False)

    def __post_init__(self):
        self.sid = ("l_" if self.is_left else "r_") + self.state.sid

    @classmethod
    def make(cls, builder, st):
        is_left = isinstance(st, ConcatState_Left)
        return cls(
            builder=builder,
            is_left=is_left,
            is_right=not is_left,
            state=st.state
        )

    @property
    def nfa1(self):
        return self.builder.fsm1.nfa
    @property
    def nfa2(self):
        return self.builder.fsm2.nfa

    @property
    def label(self):
        return generate_str(
            self.builder.left if self.is_left else self.builder.right,
            dict(_=self.state.label)
        )


    def build(self):
        return State(
            sid = self.sid,
            initial = self.is_left and self.state == self.nfa1.start_state,
            final = self.is_right and self.nfa2.accepted_states(self.state),
            highlight = (
                (self.is_left and self.nfa1.accepted_states(self.state)) or
                (self.is_right and self.nfa2.start_state == self.state)
            ),
            label=self.label,
            transitions=[],
        )

# Union

class UnionBuilder(BinOpBuilder):
    def _post_init(self, style):
        self.init_label = style.get("union", dict()).get("init_label", "q_\cup")
        self.left_tpl = style.get("union", dict()).get("left_state", "{{ _ }}")
        self.right_tpl = style.get("union", dict()).get("right_state", "{{ _ }}")
        self.init_sid = "q0"

    def binop(self, nfa1, nfa2):
        return nfa1.union(nfa2)

    def wrap_state(self, st):
        return UnionState.make(self, st)

    def get_old_tsx(self, src_dir, dst_dir):
        edge = (src_dir.state, dst_dir.state)
        if src_dir.is_left and dst_dir.is_left:
            old_tsx = self.fsm1.transitions.get(edge)
        elif src_dir.is_right and dst_dir.is_right:
            old_tsx = self.fsm2.transitions.get(edge)
        else:
            old_tsx = None
        return old_tsx

    def highlight(self, src, dst):
        return src.is_init

class UnionDir(Enum):
    LEFT = 0
    RIGHT = 1
    INIT = 2

@dataclass
class UnionState:
    builder: ConcatBuilder
    state: State
    direction: UnionDir
    sid:str = field(init=False)

    def __post_init__(self):
        if self.direction == UnionDir.LEFT:
            self.sid = "l_" + self.state.sid
        elif self.direction == UnionDir.RIGHT:
            self.sid = "r_" + self.state.sid
        else:
            self.sid = self.builder.init_sid

    @classmethod
    def make(cls, builder, st):
        (state_kind, state) = st
        return cls(
            builder=builder,
            direction=UnionDir(state_kind),
            state=state
        )

    @property
    def is_left(self):
        return self.direction == UnionDir.LEFT
    @property
    def is_right(self):
        return self.direction == UnionDir.RIGHT
    @property
    def is_init(self):
        return self.direction == UnionDir.INIT
    @property
    def nfa1(self):
        return self.builder.fsm1.nfa
    @property
    def nfa2(self):
        return self.builder.fsm2.nfa
    @property
    def label(self):
        if self.direction == UnionDir.LEFT:
            label = self.builder.left_tpl
        elif self.direction == UnionDir.RIGHT:
            label = self.builder.right_tpl
        else:
            return self.builder.init_label
        return generate_str(label, dict(_=self.state.label))

    @property
    def final(self):
        if self.is_left:
            return self.nfa1.accepted_states(self.state)
        elif self.is_right:
            return self.nfa2.accepted_states(self.state)
        else:
            return False


    def build(self):
        return State(
            sid = self.sid,
            initial = self.is_init,
            final = self.final,
            highlight = (
                self.is_init or
                (self.is_left and self.state == self.nfa1.start_state) or
                (self.is_right and self.state == self.nfa2.start_state)
            ),
            label=self.label,
            transitions=[],
        )


def nfa_star(fsm, args):
    nfa2 = fsm.nfa.star()
    new_fsm = FSM()
    initial_state = State(
        sid=generate_sid(fsm.states),
        initial=True,
        final=nfa2.accepted_states(nfa2.start_state),
        highlight=True,
        label=args.star_state,
        transitions=[],
    )
    new_fsm.add_state(initial_state)
    old_to_new = dict()
    for st in nfa2.states:
        if isinstance(st, StarState_Init):
            continue
        old_state = st.state
        old_to_new[old_state.sid] = new_state = State(
            sid=old_state.sid,
            initial=False,
            final=nfa2.accepted_states(st),
            highlight=False,
            label=old_state.label,
            transitions=[],
        )
        new_fsm.add_state(new_state)

    def get_new(st):
        if isinstance(st, StarState_Init):
            return initial_state
        else:
            return old_to_new[st.state.sid]
    def get_old(st):
        if isinstance(st, StarState_Init):
            return None
        else:
            return st.state

    for ((src, dst), chars) in nfa2.as_graph()[1].items():
        new_src = get_new(src)
        new_dst = get_new(dst)
        old_tsx = fsm.transitions.get((get_old(src), get_old(dst)))
        tsx = Transition(
            # When get_old returns None is because it's a new edge
            highlight = get_old(src) is None or get_old(dst) is None,
            # Copy the style from old edge if it exists
            style = list(old_tsx.style) if old_tsx is not None else None,
            # Copy the topath from old edge if it exists
            topath = list(old_tsx.topath) if old_tsx is not None else None,
            # Use the characters in the new FSM
            chars = list(chars),
        )
        new_fsm.add_transition(new_src, new_dst, tsx)
    return new_fsm.build_dict()

def NFAConcat():
    return BinaryOpNFA(ConcatBuilder.run)

def NFAUnion():
    return BinaryOpNFA(UnionBuilder.run)

def NFAStar():
    return UnaryOpNFA(nfa_star)
